package Negocio;

import Entidad.*;
import java.time.LocalDateTime;
import java.time.LocalTime;
import ufps.util.colecciones_seed.*;
import ufps.util.varios.ArchivoLeerURL;

public class SistemaVotacion {

    private ListaCD<Departamento> dptos = new ListaCD();
    private ListaCD<Persona> personas = new ListaCD();
    private Cola<Notificacion> notificaciones = new Cola();
    
//CONSTRUCTOR
    public SistemaVotacion(String urlDptos, String urlMunicipios, String urlPer) {
        crearDptos(urlDptos);
        crearMunicipios(urlMunicipios);
        crearPersonas(urlPer);
    }

//CREAR DEPARTAMENTOS
    private void crearDptos(String url) {
        
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for (int i = 1; i < v.length; i++) {
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
           
            //id_dpto;Nombre Departamento
            int id_dpto = Integer.parseInt(datos2[0]);
            
            Departamento nuevo = new Departamento(id_dpto, datos2[1]);
            this.dptos.insertarAlFinal(nuevo);
        }
    }

//BISCAR DEPARTAMENTOS
    private Departamento buscarDpto(int id) {
        
        for (Departamento dato : this.getDptos()) {
            if (dato.getId_dpto() == id) {
                return dato;
            }
        }
        return null;
    }

//CREAR MUNICIPIOS
    private void crearMunicipios(String url) {
        
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for (int i = 1; i < v.length; i++) {
            //id_dpto;id_municipio;nombreMunicipio    
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            
            int id_dpto = Integer.parseInt(datos2[0]);
            int id_muni = Integer.parseInt(datos2[1]);
            String nombreMun = datos2[2];
            
            Municipio nuevo = new Municipio(id_muni, nombreMun);

            Departamento dpto = buscarDpto(id_dpto);
            if (dpto != null) {
                dpto.getMunicipios().insertarAlFinal(nuevo);
            }
        }
    }

//CREAR FECHA
    private LocalDateTime crearFecha(String fecha) {

        String datos3[] = fecha.split("-");
        
        int agno = Integer.parseInt(datos3[0]);
        int mes = Integer.parseInt(datos3[1]);
        int dia = Integer.parseInt(datos3[2]);

        return LocalDateTime.of(agno, mes, dia, 0, 0, 0, 0);
    }

//CREAR PERSONAS       
    private void crearPersonas(String url) {

        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object v[] = archivo.leerArchivo();
        
        for (int i = 1; i < v.length; i++) {
            //cedula;nombre;fechanacimiento;id_municipio_inscripcion;email
            String datos = v[i].toString();
            String datos2[] = datos.split(";");
            
            long cedula = Long.parseLong(datos2[0]);
            String nombre = datos2[1];
            LocalDateTime fechaNacimiento = crearFecha(datos2[2]);
            int idMuni = Integer.parseInt(datos2[3]);
            String email = datos2[4];
            
            //Crear personas
            Persona nueva = new Persona(cedula, nombre, fechaNacimiento, idMuni, email);
            this.personas.insertarAlFinal(nueva);
        }
    }

// obtner listados 
    public String getListadoDpto() {
        String msg = "";
        for (Departamento datos : this.dptos) {
            msg += datos.toString() + "\n";
        }
        return msg;
    }

    public String getListadoPersonas() {
        String msg = "";
        for (Persona datos : this.personas) {
            msg += datos.toString() + "\n";
        }
        return msg;

    }

//get and set
    public ListaCD<Departamento> getDptos() {
        return dptos;
    }

    public void setDptos(ListaCD<Departamento> dptos) {
        this.dptos = dptos;
    }

    public ListaCD<Persona> getPersonas() {
        return personas;
    }

    public void setPersonas(ListaCD<Persona> personas) {
        this.personas = personas;
    }

    public Cola<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(Cola<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

}
